import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(myApp());

class myApp extends StatefulWidget {
  @override
  _myAppState createState() => _myAppState();
}

class _myAppState extends State<myApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My CV App",
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("My CV App"),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                elevation: 2.0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 30.0,
                    horizontal: 30,
                  ),
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.red,
                        backgroundImage: AssetImage("image/index.jpg"),
                        radius: 50.0,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Er. Amrit Kafle",
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.black,
                          fontFamily: null,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "Web and Mobile App Developer",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "Hello, it's me Amrit Kafle. i ma web and mobile app developer.",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: "Roboto",
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                  top: 10.0,
                ),
                child: Text(
                  "Social Links",
                  style: TextStyle(
                    fontFamily: "Roboto",
                    fontSize: 20.0,
                    color: Colors.green,
                  ),
                ),
              ),
              Card(
                elevation: 2.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(FontAwesomeIcons.facebook),
                          color: Colors.blue,
                          onPressed: () {
                            launch("https://www.facebook.com/immyrit.kafle");
                          }),
                      IconButton(
                          icon: Icon(FontAwesomeIcons.twitter),
                          color: Colors.blueAccent,
                          onPressed: () {
                            launch("https://www.facebook.com/immyrit.kafle");
                          }),
                      IconButton(
                          icon: Icon(FontAwesomeIcons.instagram),
                          color: Colors.red,
                          onPressed: () {
                            launch("https://www.facebook.com/immyrit.kafle");
                          }),
                      IconButton(
                          icon: Icon(FontAwesomeIcons.linkedin),
                          color: Colors.green,
                          onPressed: () {
                            launch("https://www.facebook.com/immyrit.kafle");
                          }),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 10.0,
                  top: 10.0,
                ),
                child: Text(
                  "Skils",
                  style: TextStyle(
                    fontFamily: "Roboto",
                    fontSize: 20.0,
                    color: Colors.green,
                  ),
                ),
              ),
              Card(
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: NetworkImage(
                        "https://cdn.pixabay.com/photo/2016/10/26/18/59/web-development-1772234_960_720.jpg"),
                  ),
                  title: Text("Web Developement"),
                  subtitle: Text("HTML, css, Javascript etc"),
                ),
              ),
              Card(
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: NetworkImage(
                        "https://live.staticflickr.com/4708/39593666341_4bdcdd510d_b.jpg"),
                  ),
                  title: Text("App Developement"),
                  subtitle: Text("Dart and Flutter etc"),
                ),
              ),
              Card(
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: NetworkImage(
                        "https://p0.pikist.com/photos/212/703/engineer-code-coding-software-computer-engineering-binary-tech-technology.jpg"),
                  ),
                  title: Text("Database Design"),
                  subtitle: Text("Mysql, PosGress Sql etc"),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
